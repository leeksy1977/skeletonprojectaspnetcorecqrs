# Your next best project could start here!

Ok, maybe not, but if you want a stream lined, ASP.NET Core application utilising Mediatr to fulfil the CQRS pattern.

Complete with added bonus test project, then you can do worse than start here.